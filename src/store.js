import { createStore } from "vuex";

import { apiUserNewOrExist, apiPostUser } from "./api/user";
import { apiGetQuestions } from "./api/questionDB";

export default createStore({
  state: {
    user: "",
    options: {
      amount: 0,
      difficulty: "",
      category: 0,
    },
    game: {
      questionAll: [],
      answerOptions: [],
      result: [],
      index: 0,
      score: 0,
    },
  },
  getters: {
    getGameIndex: (state) => {
      return state.game.index;
    },
    getGameLength: (state) => {
      return state.game.questionAll.length;
    },
    getQuestion: (state) => {
      console.log("getQuestionIsCalled", state.game.index);
      return state.game.questionAll[state.game.index];
    },
    getAnswerOptions: (state) => {
      console.log("getAnswerOptionsCalled");
      return state.game.answerOptions;
    },
    getResult: (state) => {
      return state.game.result;
    },
    getScore: (state) => {
      return state.game.score;
    },
  },
  mutations: {
    setUser: (state, user) => {
      state.user = user;
      console.log("mutation", user);
    },
    setOptions: (state, options) => {
      state.options = options;
    },
    setQuestionAll: (state, questionAll) => {
      console.log(questionAll);
      state.game.questionAll = questionAll;
    },
    setResult: (state, result) => {
      const resultObject = {
        question: "",
        correct: "",
        picked: "",
        point: false,
      };
      resultObject.question = result[0];
      resultObject.correct = result[1];
      resultObject.picked = result[2];
      resultObject.point = result[3];
      state.game.result.push(resultObject);
    },
    incrementGameIndex: (state) => {
      state.game.index++;
    },
    setAnswerOptions: (state, options) => {
      state.game.answerOptions = options;
    },
    setScore: (state) => {
      let points = state.game.score;
      state.game.result.forEach((element) => {
        if (element.point) {
          points = points + 10;
        }
      });
      state.game.score = points;
    },
    resetGame: (state) => {
      (state.game.questionAll = []),
        (state.game.answerOptions = []),
        (state.game.result = []),
        (state.game.index = 0),
        (state.game.score = 0);
    },
  },
  actions: {
    // Check if user exists, if so, get user and set user state. If not, create new user
    // and set user state. See comments in user.js for more info.
    async apiUserNewOrExist({ commit, state }, { username, options }) {
      let user = await apiUserNewOrExist(username.value);
      if (!user) {
        user = await apiPostUser(username.value);
        commit("setUser", user);
        return null;
      } else {
        console.log("committing mutation");
        commit("setUser", user);
        commit("setOptions", options.value);
        return null;
      }
    },
    async apiGetQuestions({ commit, state }) {
      let questionAll = await apiGetQuestions(state.options);
      if (!questionAll) {
        return null;
      } else {
        console.log("committing questions");
        commit("setQuestionAll", questionAll);
        return null;
      }
    },
  },
});
