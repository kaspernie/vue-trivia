# Vue Trivia Game

This is the seconde assignment in the Noroff's JavaScript module. See [vue_trivia_game_specifications.pdf](./vue_trivia_game_specifications.pdf).

The component tree is found in [vue_trivia_game_component_tree.pdf](./vue_trivia_game_component_tree.pdf)

## Install
Clone or download repository. For running the app locally, install Node.js and necessary npm modules (eg. `npm install`) and run `npm run dev`.

## Usage

The app is deployed live at:
[https://sleepy-sands-85904.herokuapp.com/](https://sleepy-sands-85904.herokuapp.com/)

Alternatively, view public/index.html in a modern web browser by either
- serving the file on a remote webserver
- serving the file on a local webserver
- open the file directly from a local drive

## Maintainers

Filip Hein
[https://gitlab.com/HEINF]

Kasper Nielsen
[https://gitlab.com/kaspernie]

Marius Samson
[https://gitlab.com/Maxius0]
